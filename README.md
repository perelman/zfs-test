# zfs-test

Scripts for trying out how ZFS works in some simple scenarios.

I ran these scripts on a VM. They will modify the ZFS pool named `tank`,
which is the default pool name in ZFS examples, so may be a valid ZFS
pool name on your system.

```sh
# Create a test pool named tank backed by a the file /root/tank
fallocate -l 1G /root/tank
zpool create tank /root/tank

cd zfs-test/bin
# dump logs from create-and-measure / copy-all-and-measure into ../logs/
./measure-all
# read ../logs/ and print Markdown table of dataset space used below
./logs-to-table --links
```

The rows are which script from `bin/create-test-setup/` was used to
create the `tank/test` dataset. The columns are which script from
`bin/copy-snapshot/` was used to copy each snapshot into `tank/target`.
The cells are the space used by the `tank/target` (or `test/test` for
the first column) dataset for that combination.

| Create script | *orig* | [rsync-ahvx](bin/copy-snapshot/rsync-ahvx) | [rsync-ahvx-sparse](bin/copy-snapshot/rsync-ahvx-sparse) | [rsync-inplace](bin/copy-snapshot/rsync-inplace) | [rsync-inplace-no-whole-file](bin/copy-snapshot/rsync-inplace-no-whole-file) | [rsync-no-whole-file](bin/copy-snapshot/rsync-no-whole-file) | [zfs-diff-move-then-rsync](bin/copy-snapshot/zfs-diff-move-then-rsync) |
| :--- | ---: | ---: | ---: | ---: | ---: | ---: | ---: |
| [edit-part-of-file](bin/create-test-setup/edit-part-of-file) | **1.16M** | 2.04M❌ | 2.04M❌ | 2.04M❌ | 1.17M✅ | 2.04M❌ | 1.17M✅ |
| [empty](bin/create-test-setup/empty) | **24K** | 24K✅ | 24K✅ | 24K✅ | 24K✅ | 24K✅ | 24K✅ |
| [encrypted-nested](bin/create-test-setup/encrypted-nested) | **1.84M** | 24K✅ | 24K✅ | 24K✅ | 24K✅ | 24K✅ | 24K✅ |
| [move-file](bin/create-test-setup/move-file) | **1.04M** | 2.04M❌ | 2.04M❌ | 2.04M❌ | 2.04M❌ | 2.04M❌ | 1.04M✅ |
| [random-1M-file](bin/create-test-setup/random-1M-file) | **1.03M** | 1.03M✅ | 1.03M✅ | 1.03M✅ | 1.03M✅ | 1.03M✅ | 1.03M✅ |
| [zeros-1M-file](bin/create-test-setup/zeros-1M-file) | **24K** | 1.03M❌ | 24K✅ | 1.03M❌ | 1.03M❌ | 1.03M❌ | 1.03M❌ |
